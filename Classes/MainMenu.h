#pragma execution_character_set("UTF-8")
#ifndef __MAINMENU_H__
#define __MAINMENU_H__

#include "cocos2d.h"

class MainMenu : public cocos2d::Layer
{
public:
    


    static cocos2d::Scene* createScene();
    
    cocos2d::Sprite* ship;

   

    virtual bool init();

	void GoToGameScene(Ref *pSender);
	void GoToChangeLevel(Ref *pSender);
	void GoToInfoScene(Ref *pSender);
	cocos2d::__Dictionary *dictionary;
	

    CREATE_FUNC(MainMenu);
};

#endif // __MainMenu_H__
