#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "string"
#include "PauseScene.h"
#include "SimpleAudioEngine.h"
#include "string.h"
#include "KeyboardMap.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "GameOverScene.h"
#include <math.h>
#include <algorithm>
#include "ChangeLevel.h"


USING_NS_CC;

int level_of_train;
Scene* GameScene::createScene(int lv)
{
	level_of_train = lv;

	log("Level %i", level_of_train);
    auto scene = Scene::create();
    auto layer = GameScene::create();
    scene->addChild(layer);

    return scene;
}

using namespace std;

string utf8_substr(const string& str, unsigned int start = 0, unsigned int leng = string::npos);

bool GameScene::init()
{

	if (!Layer::init())
	{
		return false;
	}

	Size size = Director::getInstance()->getWinSize();

    int lv = level_of_train;

	if(lv != 0) {

    	    isTraining = true;


            wordNeed += wordIncrease * lv;


            speedLevel = ((speedLevel - (lv - 1)) > speedBottomLevel) ? (speedLevel - (lv - 1)) : speedBottomLevel;
            double dfWT = (speedLevel <= speedBottomLevel) ? ((lv - speedLevel) * eraseSpeed) : wordTime;

            if(speedLevel <= speedBottomLevel)
                wordTime -= (dfWT <= wordTime) ? dfWT : 0;

            log("SL %d", speedLevel);
            log("WT %f", wordTime);

            std::string levelS = "Тренування ";
            levelS += std::to_string(lv);

//            level->setString(levelS);

    	} else
    	    isTraining = false;

    // Randomize words

    log("Site original %d", articleF.size());

    while(articleF.length() > 4) {

        int space = articleF.find(' ');
        int str_len2 = articleF.length();

        std::string word = articleF.substr(0, space + 1);
        articleF = articleF.substr(space + 1, str_len2 - space);

        sortWords.push_back(word);

    }

    articleF = "";
    for(int i = 0; i < sortWords.size(); i++) {

         int x = rand() % (sortWords.size() - 1);
        // log("%d", x);

         articleF += sortWords.at(x);
//         articleF += ' ';


//         auto i = std::find(begin(sortWords), end(sortWords), 0);
////         sortWords.erase(sortWords.begin());
         //sortWords.erase(std::remove(sortWords.begin(), sortWords.end(), 0), sortWords.end());

    }
    articleF += ' ';

    //log("%s", articleF.c_str());

    log("Site before %d", articleF.size());

    std::string newW = "";
    newW += ' ';
    std::string articlePart = "";
    articlePart += ' ';

    for(int i = 0; i < 2; i++) {

        int lengthPart = article.length();
        int lengthPartStart;
        if(lengthPart < 80) {
            lengthPartStart = 0;
        } else {
            lengthPartStart = lengthPart - 80;
        }

        articlePart = "";
        articlePart += ' ';
        articlePart += article.substr(lengthPartStart, lengthPart);
//        log("%s", articlePart.c_str());

        while(newW.length() < 200) {

            int space = articleF.find(' ');
            int str_len = articleF.length();

            std::string word = articleF.substr(0, space + 1);
            articleF = articleF.substr(space + 1, str_len - space);



            string checkWord = "";
            checkWord += ' ';
            checkWord += utf8_substr(word, 0, 1);
//            log("%s", checkWord.c_str());

            if(newW.find(checkWord) == -1 && articlePart.find(checkWord) == -1) {
                newW += word;
            } else {
//                newW += word;
//                log("%s", newW.c_str());
//                log("%d", newW.length());
//                log("%s", "yes");
            }




        }
        article += newW;
        newW = "";
    }

    int str_len = article.length();
    article = article.substr(1, str_len);
    log("Site after %d", article.size());

    articleF = article;


	auto background = Sprite::create("space.jpg");
	background->setPosition(Vec2(size.width / 2, size.height / 2));
	float rX = size.width / background->getContentSize().width;
	float rY = size.height / background->getContentSize().height;
	background->setScaleX(rX);
	background->setScaleY(rY);
	this->addChild(background);

	
	 // Score label

    score = Label::createWithSystemFont("0000", "Arial", 20);
    score->setPosition(Vec2(20, size.height - 20));
    score->setAnchorPoint(Vec2(0, 1));
    this->addChild(score);


    std::string levelST = "Тренування Р";
    levelST += std::to_string(lv);

    level = (isTraining) ? Label::createWithSystemFont(levelST, "Arial", 20) : Label::createWithSystemFont("Рівень 1", "Arial", 20);
    level->setPosition(Vec2(20, size.height - 50));
    level->setAnchorPoint(Vec2(0, 1));
    this->addChild(level);

	ship = Sprite::create("ship.png");
	ship->setScale(0.5);
	ship->setPosition(Vec2(size.width / 2, 50));
	this->addChild(ship, 100);
	auto menuItem1 = MenuItemImage::create(
		"pause.png",
		"pause_hover.png",
		CC_CALLBACK_1(GameScene::GoToPauseScene, this));

    auto gh = Sprite::create("gamehub.png");
    gh->setPosition(Vec2(50, 44));
    gh->setScale(0.25f);
    this->addChild(gh);


	auto menu2 = Menu::create(menuItem1, NULL);
	menu2->setScale(0.2f);
	menu2->setPosition(Vec2(size.width/1.58f - 50, -250));
	this->addChild(menu2); 


	auto delayTimeAction = DelayTime::create(1);

	delayFunc();



	auto eventListener = EventListenerKeyboard::create();

	eventListener->onKeyPressed = [&](EventKeyboard::KeyCode keyCode, Event* event) {

		Vec2 loc = event->getCurrentTarget()->getPosition();
		std::string keyLetter = KeyBoardMap::getInstance()->getKeyCodeInStr(keyCode);


		if (checkFirstLetter) {
			for (Label* labelTitle : wordsHidden) {
				std::string word = labelTitle->getString();

				if (utf8_substr(word, 0, 1).c_str() == keyLetter) {

					labelTitle->setString(utf8_substr(word, 1, word.length() - 1));

                    focusWord = labelTitle;
                    checkFirstLetter = false;

                    int index = wordsHidden.getIndex(labelTitle);
                    wordDestroy = words.at(index);


                    angle(labelTitle);
                    shot(labelTitle, word);

                    changeScore(true);
				}
			}
			if (checkFirstLetter) {

				changeScore(false);
			}
		}
		else {

			std::string word = focusWord->getString();
			if (utf8_substr(word, 0, 1).c_str() == keyLetter) {

				focusWord->setString(utf8_substr(word, 1, word.length() - 1));

                angle(focusWord);

				if (word.length() <= 3) {
					checkFirstLetter = true;
					int index = wordsHidden.getIndex(focusWord);
                    wordCount++;

				}


                shot(focusWord, word);

                changeScore(true);

			}
			else {
				changeScore(false);
			}
		}

	};

	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener, ship);
	this->scheduleUpdate();


	return true;
}

void GameScene::addWordOnScene() {
	printf("CARD NAME %s\n", words.at(0)->getString().c_str());
	this->words.at(0)->setScale(1.3f);
}

void GameScene::delayFunc()
{
	float delay = speed;
	this->schedule(schedule_selector(GameScene::actionAfterDelay), delay);
}
void GameScene::actionAfterDelay(float dt)
{
    speedCount++;
    if(speedCount == speedLevel) {

        speedCount = 0;

        log("wordTime %f", wordTime);
        log("SPL %d", speedLevel);

        Size size = Director::getInstance()->getWinSize();


        if(article.length() < 5)
            article = articleF;


        int str_len = article.length();

        if (str_len > 0) {

            bool isSame = false;
            std::string word;

//            do {

                int space = article.find(' ');
                word = article.substr(0, space + 1);
                article = article.substr(space + 1, str_len - space);

                isSame = false;

//                for (Label* labelTitle : wordsHidden) {
//                    if(labelTitle->getString().substr(0, 1) == word.substr(0, 1)) {
//                        isSame = true;
//                        article += word;
//                        break;
//                    }
//                }

//            } while(isSame);

                speed -= (speed > 0.8) ? 0.1 : 0;
                this->delayFunc();

            // words separate

            // rand for horizontal position if labels

            int max = size.width;
            int min = 0;

            int randPosX = rand() % (max - min) + min;

            // generate and move label
            std::transform(word.begin(), word.end(), word.begin(), ::tolower);
            auto myLabel = Label::createWithSystemFont(word.c_str(), "Arial", 16);
            myLabel->setPosition(Vec2(randPosX, size.height));
            GameScene::addChild(myLabel);
            this->words.pushBack(myLabel);

            auto myLabel2 = Label::createWithSystemFont(word.c_str(), "Arial", 16);
            myLabel2->setPosition(Vec2(randPosX, size.height));
//            GameScene::addChild(myLabel2);
            this->wordsHidden.pushBack(myLabel2);

            auto moveTo = MoveTo::create(wordTime, Vec2(ship->getPositionX(), ship->getPositionY()));
            myLabel->runAction(moveTo);

        }

    }
}

void GameScene::shot(cocos2d::Label* label, std::string word)
{

    auto piy = Sprite::create("shot2.png");
	piy->setPosition(Vec2(ship->getPositionX(), ship->getPositionY() + 7));
	//piy->setColor(Color3B(255,253,8));
	GameScene::addChild(piy, 1);


    int x1 = ship->getPositionX(),
        x2 = label->getPositionX(),
        y1 = ship->getPositionY(),
        y2 = label->getPositionY();

    posXM = x2 / 15;
    posYM = y2 / 15;

    float angle = atan2(y2 - y1, x2 - x1) * 180/3.14159265;
    piy->setRotation(90 - angle);


    shotItems.pushBack(piy);
    int index = wordsHidden.getIndex(label);

    shotItemsLabel.pushBack(words.at(index));

}


void GameScene::update(float dt)
{

    if(wordNeed == wordCount && !isTraining) {

        levelNumber++;

        wordCount = 0;
        wordNeed += wordIncrease;

        speedCount = 0;
        speedLevel -= (speedLevel != speedBottomLevel) ? 1 : 0;
        wordTime -= (speedLevel <= speedBottomLevel) ? eraseSpeed : 0;


        if(!isTraining) {
            std::string levelS = "Рівень ";
            levelS += std::to_string(levelNumber);

            level->setString(levelS);
        }
    }

    for (Label* labelTitle : words) {
        Vec2 posWord = labelTitle->getPosition();
        if (posWord.x == ship->getPositionX() && posWord.y == ship->getPositionY()) {
            auto tintTo = TintTo::create(0.2f, 255.0f, 0.0f, 0.0f);
            ship->runAction(tintTo);
            log("%s", "Oh no, you fail");
            log("Good press: %d", goodPress);
            log("Bad press: %d", badPress);
            GoToGameOverScene(this);
            stop = true;
        }
    }


        if(shotItems.size() > 0) {

            int i = 0;
            int currentX = shotItems.at(i)->getPositionX();
            int currentY = shotItems.at(i)->getPositionY();

            shotItems.at(i)->setScale(shotItems.at(i)->getScale() - 0.04);

            int newX = shotItemsLabel.at(i)->getPositionX();
            int newY = shotItemsLabel.at(i)->getPositionY();
            shotItems.at(i)->setPosition(Vec2(currentX + posXM - 16, currentY + posYM));


            if (shotItems.at(i)->getPositionY() >= shotItemsLabel.at(i)->getPositionY()) {

                auto fadeIn = FadeIn::create(0);
                auto fadeOut = FadeOut::create(0.2);

                boom = Sprite::create("shotend.png");
                boom->setPosition(Vec2(shotItemsLabel.at(i)->getPositionX() - 7, shotItemsLabel.at(i)->getPositionY() - 7));
                boom->setScale(0.8f);
                GameScene::addChild(boom, 120);
                boom->runAction(Sequence::create(fadeIn, fadeOut, nullptr));



                std::string word = shotItemsLabel.at(i)->getString();

                shotItemsLabel.at(i)->setString(utf8_substr(word, 1, word.length() - 1));


                if (word.length() <= 3) {

                    checkFirstLetter = true;

                    int index = words.getIndex(shotItemsLabel.at(i));
                    GameScene::removeChild(shotItemsLabel.at(i), true);
                    shotItemsLabel.at(i)->removeFromParentAndCleanup(true);

                    GameScene::removeChild(wordsHidden.at(index), true);
                    wordsHidden.at(index)->removeFromParentAndCleanup(true);
					wordsHidden.erase(index);

                    GameScene::removeChild(words.at(index), true);
                    words.at(index)->removeFromParentAndCleanup(true);
					words.erase(index);
                }

                auto tintTo = TintTo::create(0.1f, 255.0f, 0.0f, 0.0f);
                auto tintToWhite = TintTo::create(0.1f, 255.0f, 255.0f, 255.0f);
                shotItemsLabel.at(i)->runAction(Sequence::create(tintTo, tintToWhite, nullptr));

                GameScene::removeChild(shotItems.at(i), true);
                shotItems.at(i)->removeFromParentAndCleanup(true);

                this->shotItems.erase(shotItems.begin());
                this->shotItemsLabel.erase(shotItemsLabel.begin());


            }

    }

}


string utf8_substr(const string& str, unsigned int start, unsigned int leng)
{
	if (leng == 0) { return ""; }
	unsigned int c, i, ix, q, min = string::npos, max = string::npos;
	for (q = 0, i = 0, ix = str.length(); i < ix; i++, q++)
	{
		if (q == start) { min = i; }
		if (q <= start + leng || leng == string::npos) { max = i; }

		c = (unsigned char)str[i];
		if (c >= 0 && c <= 127) i += 0;
		else if ((c & 0xE0) == 0xC0) i += 1;
		else if ((c & 0xF0) == 0xE0) i += 2;
		else if ((c & 0xF8) == 0xF0) i += 3;
		//else if (($c & 0xFC) == 0xF8) i+=4; // 111110bb //byte 5, unnecessary in 4 byte UTF-8
		//else if (($c & 0xFE) == 0xFC) i+=5; // 1111110b //byte 6, unnecessary in 4 byte UTF-8
		else return "";//invalid utf8
	}
	if (q <= start + leng || leng == string::npos) { max = i; }
	if (min == string::npos || max == string::npos) { return ""; }
	return str.substr(min, max);
}

void GameScene::changeScore(bool add)
{
    if(add)
    {
        goodPress++;
        totalScore += coefficientRightPress;
    }
    else
    {
        badPress++;
        totalScore = (totalScore >= 2) ? totalScore - coefficientBadPress : 0;
    }
    score->setString(std::to_string(totalScore));
}

void GameScene::angle(cocos2d::Label* label)
{
    int x1 = ship->getPositionX(),
        x2 = label->getPositionX(),
        y1 = ship->getPositionY(),
        y2 = label->getPositionY();

    posXM = x2 / 15;
    posYM = y2 / 15;

    float angle = atan2(y2 - y1, x2 - x1) * 180/3.14159265;
    ship->setRotation(90 - angle);

}

void GameScene::GoToPauseScene(cocos2d::Ref *pSender)
{
	auto scene = PauseScene::createScene();
	Director::getInstance()->pushScene(scene);
}
void GameScene::GoToGameOverScene(cocos2d::Ref *pSender)
{
	Scene *tempScene = GameOverScene::createScene(totalScore, goodPress, badPress, isTraining);
	Director::getInstance()->replaceScene(tempScene);
}