#ifndef __PAUSESCENE_H__
#define __PAUSESCENE_H__

#include "cocos2d.h"

class PauseScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    
   

    virtual bool init();

	void Resume(Ref *pSender);
	void GoToMainMenu(Ref *pSender);

    CREATE_FUNC(PauseScene);
};

#endif // __MainMenu_H__
