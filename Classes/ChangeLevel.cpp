#include "SimpleAudioEngine.h"
#include "string"
#include "GameScene.h"
#include "MainMenu.h"
#include "ChangeLevel.h"
//#include "ui/CocosGUI.h"
//#include"extensions/cocos-ext.h"
USING_NS_CC;
//USING_NS_CC_EXT;
//using namespace ui;

Scene* ChangeLevel::createScene()
{
    auto scene = Scene::create();
    auto layer = ChangeLevel::create();
    scene->addChild(layer);

    return scene;
}

bool ChangeLevel::init()
{

    if ( !Layer::init() )
    {
        return false;
    }

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Size size = Director::getInstance()->getWinSize();

	auto background = Sprite::create("space.jpg");
	background->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	float rX = visibleSize.width / background->getContentSize().width;
	float rY = visibleSize.height / background->getContentSize().height;
	background->setScaleX(rX);
	background->setScaleY(rY);
	this->addChild(background);

	auto label1 = Label::createWithSystemFont("Ранер", "", 34,	Size(300, 100), TextHAlignment::LEFT, TextVAlignment::CENTER);
	label1->setColor(Color3B::YELLOW);
	label1->setPosition(Vec2(size.width/3, 600));
	this->addChild(label1);

	auto label2 = Label::createWithSystemFont("Тренування", "", 34, Size(300, 100), TextHAlignment::LEFT, TextVAlignment::CENTER);
	label2->setColor(Color3B::YELLOW);
	label2->setPosition(Vec2(size.width / 3, 380));
	this->addChild(label2);

	MenuItemFont::setFontSize(40);
	auto menuItemFirst = MenuItemFont::create("Полетіли!!!", CC_CALLBACK_1(ChangeLevel::GoToGameScene, this));
	auto menuCenter1 = Menu::create(menuItemFirst, NULL);
	menuCenter1->setPosition(Vec2(size.width / 2.0, 487 ));
	menuCenter1->setColor(Color3B::WHITE);
	menuCenter1->alignItemsVerticallyWithPadding(40.0f);
	this->addChild(menuCenter1, 2);

	auto gomenu = MenuItemFont::create("- меню -", CC_CALLBACK_1(ChangeLevel::GoToMainMenu, this));
	auto menu = Menu::create(gomenu, NULL);
	menu->setPosition(Vec2(visibleSize.width / 2.0, visibleSize.height / 7.5));
	menu->setColor(Color3B(226, 206, 55));
	this->addChild(menu);

	editName = ui::EditBox::create(Size(115, 70), ui::Scale9Sprite::create("button.png"));
	editName->setPosition(Vec2(visibleSize.width / 2.5 , 260 ));
	editName->setFontSize(35);
	editName->setFontColor(Color3B::YELLOW);

	editName->setPlaceHolder("Рівень");
	editName->setReturnType(ui::EditBox::KeyboardReturnType::GO);
	editName->setPlaceholderFontColor(Color3B::WHITE);
	editName->setPlaceholderFontSize(28);
	editName->setMaxLength(2);
	this->addChild(editName);

	auto line1 = Sprite::create("button_button1.png");
	line1->setPosition(Vec2(size.width /7, 588));
	this->addChild(line1);

	auto line2 = Sprite::create("button_button.png");
	line2->setPosition(Vec2(size.width / 4.5, 375));
	this->addChild(line2);

	auto play = MenuItemImage::create(
		"play_lv.png",
		"play_hover.png",
		CC_CALLBACK_1(ChangeLevel::GoToGameScene, this));

	auto menu2 = Menu::create( play, NULL);
	menu2->setScale(0.2f);
	menu2->setPosition(Vec2(visibleSize.width / 4.5, -23));
	this->addChild(menu2);
	
	this->scheduleUpdate();
	
    return true;
}

void ChangeLevel::update(float dt)

{
	/*std::string str = editName->getText();
	//log("String %s ", str.c_str());
	a = atoi(str.c_str());
	log("Int %i ", a);
	//a = (int)editName->getText();
	//log("String %s ",a);*/
}
void ChangeLevel::GoToMainMenu(Ref *pSender)
{
	auto scene = MainMenu::createScene();
	Director::getInstance()->replaceScene(scene);
	/*auto scene = MainMenu::createScene();
	Director::getInstance()->replaceScene(scene);*/
}
void ChangeLevel::GoToGameScene(Ref *pSender)
{
	std::string str = editName->getText();
	
	if(str!="") {
		//log("String %s ", str.c_str());
		a = atoi(str.c_str());
		//log("Int %i ", a);
	}
	//else
	//	log("Int %i ", a);
	
	
	
	Scene *tempScene = GameScene::createScene(a);
	Director::getInstance()->replaceScene(tempScene);
}

/*void ChangeLevel::editBoxEditingDidBegin(ui::EditBox* editBox) {
	
	log("Began");
}

void ChangeLevel::editBoxEditingDidEnd(ui::EditBox* editBox) {

	log("End");
}


void ChangeLevel::editBoxTextChanged(ui::EditBox* editBox, const std::string& text) {
	log("Text changed, text : %s", text.c_str());
}


void ChangeLevel::editBoxReturn(ui::EditBox* editBox) {
	GoToGameScene(this);
}*/


/*void ChangeLevel::editBoxEditingDidEndWithAction(EditBox* editBox, EditBoxEndAction action) {
	log("Returned");
};*/