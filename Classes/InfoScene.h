#ifndef __INFOSCENE_H__
#define __INFOSCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

//#include "cocos-ext.h"
//using namespace cocos2d;
//using namespace cocos2d::extension;
//using namespace std;
class InfoScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
	cocos2d::Sprite* ship;
   

    virtual bool init();

	
	void GoToMainMenu(Ref *pSender);
	void textFieldEvent(Ref *pSender, cocos2d::ui::TextField::EventType type);

    CREATE_FUNC(InfoScene);
};

#endif // __MainMenu_H__
