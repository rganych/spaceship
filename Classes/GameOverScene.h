#ifndef __GAMEOVERSCENE_H__
#define __GAMEOVERSCENE_H__

#include "cocos2d.h"
#pragma execution_character_set("UTF-8")

class GameOverScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene(int score,int good, int bad, bool isTraining);
    
   

    virtual bool init();
	void GoToMainMenu(Ref *pSender);
	cocos2d::Label *score;
	cocos2d::Label *percentage;
	

    CREATE_FUNC(GameOverScene);
};

#endif // __GAMEOVERSCENE_H__
