#include "PauseScene.h"
#include "SimpleAudioEngine.h"
#include "string"
#include "GameScene.h"
#include "MainMenu.h"

USING_NS_CC;

Scene* PauseScene::createScene()
{
    auto scene = Scene::create();
    auto layer = PauseScene::create();
    scene->addChild(layer);

    return scene;
}

bool PauseScene::init()
{

    if ( !Layer::init() )
    {
        return false;
    }

	Size size = Director::getInstance()->getWinSize();

	auto background = Sprite::create("space.jpg");
	background->setPosition(Vec2(size.width / 2, size.height / 2));
	float rX = size.width / background->getContentSize().width;
	float rY = size.height / background->getContentSize().height;
	background->setScaleX(rX);
	background->setScaleY(rY);
	this->addChild(background);

	
	auto menuItem1 = MenuItemImage::create(
		"main_menu.png",
		"main_menu_hover.png",
		CC_CALLBACK_1(PauseScene::GoToMainMenu, this));

	auto menuItem2 = MenuItemImage::create(
		"play.png",
		"play_hover.png",
		CC_CALLBACK_1(PauseScene::Resume, this));


	auto menu2 = Menu::create(menuItem1, menuItem2, NULL);
	menu2->setScale(0.4f);
	menu2->setPosition(Vec2(size.width / 5, size.height / 6.0));
	menu2->alignItemsHorizontallyWithPadding(30.0f);
	this->addChild(menu2);

	auto gh = Sprite::create("gamehub.png");
    gh->setPosition(Vec2(50, 44));
    gh->setScale(0.25f);
    this->addChild(gh);
	
    return true;
}

void PauseScene::GoToMainMenu(Ref *pSender)
{
	auto scene = MainMenu::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(scene);
	/*auto scene = MainMenu::createScene();
	Director::getInstance()->replaceScene(scene);*/
}
void PauseScene::Resume(cocos2d::Ref *pSender)
{
	Director::getInstance()->popScene();
	//auto scene = GameScene::createScene();
	//Director::getInstance()->popScene();
	//Director::getInstance()->replaceScene(scene);
}


