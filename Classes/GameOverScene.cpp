#include "GameOverScene.h"
#include "SimpleAudioEngine.h"
#include "string.h"
#include "string"
#include "GameScene.h"
#include "MainMenu.h"






USING_NS_CC;

int totalScore,goodPress,badPress;
bool isTrain;

Scene* GameOverScene::createScene(int score, int good, int bad, bool isTraining)
{
    isTrain = isTraining;
	totalScore = score;
	goodPress = good;
	badPress = bad;
	auto scene = Scene::create();
	auto layer = GameOverScene::create();
	scene->addChild(layer);

	return scene;
}

using namespace std;



bool GameOverScene::init()
{

	if (!Layer::init())
	{
		return false;
	}


	Size size = Director::getInstance()->getWinSize();

	auto background = Sprite::create("space.jpg");
	background->setPosition(Vec2(size.width / 2, size.height / 2));
	float rX = size.width / background->getContentSize().width;
	float rY = size.height / background->getContentSize().height;
	background->setScaleX(rX);
	background->setScaleY(rY);
	this->addChild(background);

	int perc = 0;

	if (goodPress > 0) {
		int totalSum = goodPress + badPress;
		perc = (goodPress * 100) / totalSum;
}
	percentage = Label::createWithSystemFont("Акуратність:    " + std::to_string(perc)+"%", "", 30);
	percentage->setPosition(Vec2(size.width / 2 , size.height/2 + size.height / 4.5f));
	percentage->setColor(Color3B(226, 206, 55));
	
	this->addChild(percentage);

	score = Label::createWithSystemFont("Кількість очок: "+std::to_string(totalScore), "fonts/Arial.ttf", 30);
	score->setPosition(Vec2(size.width /2 , size.height/2 + size.height/3));
	score->setColor(Color3B(226, 206, 55));
	this->addChild(score);
	if(!isTrain) {
	__Dictionary *dictonary = __Dictionary::create();

	__String *fileName = __String::create("file2.plist");
	__String *filePath = __String::createWithFormat("%s%s", FileUtils::getInstance()->getWritablePath().c_str(), fileName->getCString());
	__Dictionary *dictionary = __Dictionary::createWithContentsOfFile(filePath->getCString());
	
	if (dictionary->valueForKey("Score")->intValue() < totalScore) {
		//log("yes");
		dictonary->setObject(__Integer::create(totalScore), __String::create("Score")->getCString());
		dictonary->writeToFile(filePath->getCString());

	}
	__Dictionary *dictionary1 = __Dictionary::createWithContentsOfFile(filePath->getCString()); 
	int s= dictionary1->valueForKey("Score")->intValue();
	auto Score = Label::createWithSystemFont("        Кращий\nрезультат" , "fonts/Arial.ttf", 35);
	Score->setPosition(Vec2(size.width / 2, size.height / 2.2));
	Score->setColor(Color3B(225, 180, 60));
	this->addChild(Score);

	
	auto digital = Label::createWithTTF(std::to_string(s), "fonts/Sunflower-Medium.ttf", 65);
	digital->setPosition(Vec2(size.width / 2, size.height / 3.1));
	digital->setColor(Color3B(225, 170, 60));
	this->addChild(digital);
    }



	MenuItemFont::setFontSize(30);
	auto menuItemFirst = MenuItemFont::create("- меню -", CC_CALLBACK_1(GameOverScene::GoToMainMenu, this));
	auto menuCenter = Menu::create(menuItemFirst, NULL);
	menuCenter->setPosition(Vec2(size.width / 2.0, size.height / 7.0));
	menuCenter->setColor(Color3B(226, 206, 55));
	menuCenter->alignItemsVerticallyWithPadding(40.0f);

	this->addChild(menuCenter, 1);

	auto gh = Sprite::create("gamehub.png");
    gh->setPosition(Vec2(50, 44));
    gh->setScale(0.25f);
    this->addChild(gh);

	return true;
}
void GameOverScene::GoToMainMenu(Ref *pSender)
{
	auto scene = MainMenu::createScene();
	Director::getInstance()->replaceScene(scene);
}

