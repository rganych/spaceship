#include "PauseScene.h"
#include "SimpleAudioEngine.h"
#include "string"
#include "GameScene.h"
#include "MainMenu.h"
#include "InfoScene.h"
#include "ui/CocosGUI.h"


USING_NS_CC;

Scene* InfoScene::createScene()
{
    auto scene = Scene::create();
    auto layer = InfoScene::create();
    scene->addChild(layer);

    return scene;
}

bool InfoScene::init()
{

    if ( !Layer::init() )
    {
        return false;
    }

	Size size = Director::getInstance()->getWinSize();

	auto background = Sprite::create("space.jpg");
	background->setPosition(Vec2(size.width / 2, size.height / 2));
	float rX = size.width / background->getContentSize().width;
	float rY = size.height / background->getContentSize().height;
	background->setScaleX(rX);
	background->setScaleY(rY);
	this->addChild(background);

	
	

	
	auto sprite = Sprite::create("info1.png");
	sprite->setPosition(Vec2(size.width /2, size.height / 2 + 50));
	float X = size.width / sprite->getContentSize().width;
	sprite->setScaleX(X);
	this->addChild(sprite);

	
	MenuItemFont::setFontSize(30);

	auto menuItemFirst = MenuItemFont::create("- меню -", CC_CALLBACK_1(InfoScene::GoToMainMenu, this));

	auto menuCenter = Menu::create(menuItemFirst, NULL);
	menuCenter->setPosition(Vec2(size.width / 2.0, size.height / 7.0));
	menuCenter->setColor(Color3B(226, 206, 55));
	this->addChild(menuCenter);


	/*ui::TextField *fieldLevel = ui::TextField::create("Input text here", "arial.ttf", 20);
	fieldLevel->setMaxLength(2);
	fieldLevel->setMaxLengthEnabled(true);

	fieldLevel->setPosition(Vec2(200, 200));
	fieldLevel->addEventListener(CC_CALLBACK_2(InfoScene::textFieldEvent, this));
	fieldLevel->setTextHorizontalAlignment(cocos2d::TextHAlignment::CENTER);
	fieldLevel->setTextVerticalAlignment(cocos2d::TextVAlignment::CENTER);
	fieldLevel->setColor(Color3B(100, 100, 100));
	fieldLevel->setTouchAreaEnabled(true);
	fieldLevel->setTouchSize(Size(100, 80));
	
	this->addChild(fieldLevel);

	std::string enteredData = fieldLevel->getString();
	log("%s", enteredData.c_str());*/
	auto gh = Sprite::create("gamehub.png");
	gh->setPosition(Vec2(50, 44));
	gh->setScale(0.25f);
	this->addChild(gh);

    return true;
}

void InfoScene::GoToMainMenu(Ref *pSender)
{
	auto scene = MainMenu::createScene();
	Director::getInstance()->replaceScene(scene);
}
/*void InfoScene::textFieldEvent(Ref *pSender, cocos2d::ui::TextField::EventType type){
	ui::TextField *fieldLevel = dynamic_cast<ui::TextField *>(pSender);

	switch (type)
	{
	case cocos2d::ui::TextField::EventType::ATTACH_WITH_IME:
		fieldLevel->setColor(Color3B::YELLOW);
		break;
	case cocos2d::ui::TextField::EventType::DETACH_WITH_IME:
		fieldLevel->setColor(Color3B(100, 100, 100));
		log("%s", fieldLevel->getString().c_str());
		break;
	case cocos2d::ui::TextField::EventType::INSERT_TEXT:
		break;
	case cocos2d::ui::TextField::EventType::DELETE_BACKWARD:
		break;
	default:
		break;
	}

}*/




