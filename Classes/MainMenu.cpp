#include "MainMenu.h"
#include "SimpleAudioEngine.h"
#include "string.h"
#include "string"
#include "GameScene.h"
#include "InfoScene.h"
#include "ChangeLevel.h"




USING_NS_CC;

Scene* MainMenu::createScene()
{
    auto scene = Scene::create();
    auto layer = MainMenu::create();
    scene->addChild(layer);

    return scene;
}

using namespace std;

string utf8_substr(const string& str, unsigned int start=0, unsigned int leng=string::npos);

bool MainMenu::init()
{

    if ( !Layer::init() )
    {
        return false;
    }

	
	Size size = Director::getInstance()->getWinSize();

	auto background = Sprite::create("space.jpg");
	background->setPosition(Vec2(size.width / 2, size.height / 2));
	float rX = size.width / background->getContentSize().width;
	float rY = size.height / background->getContentSize().height;
	background->setScaleX(rX);
	background->setScaleY(rY);
	this->addChild(background);

	auto logo = Sprite::create("ship_logo.png");
    	logo->setPosition(Vec2(size.width / 2, size.height / 2 + 140));
    	logo->setScale(1.3f);
    	this->addChild(logo);

	auto menuItem1 = MenuItemImage::create(
                                               "1_1.png",
                                               "1_2.png",
                                            CC_CALLBACK_1(MainMenu::GoToChangeLevel, this));
                                            //CC_CALLBACK_1(MainMenu::GoToGameScene, this));
       menuItem1->setScale(1.3f);




      auto menuItem2 = MenuItemImage::create(
                          "3_1.png",
                          "3_2.png",
                          CC_CALLBACK_1(MainMenu::GoToInfoScene, this));




        auto menu = Menu::create(menuItem1,menuItem2, NULL);
        menu->setPosition(Vec2(size.width / 2.0, size.height / 2.3 - 100));
//        menu->setScale(1.2f);
        menu->setAnchorPoint(Vec2(0.5, 0.5));
        menu->alignItemsVerticallyWithPadding(50);
        this->addChild(menu);

	/*MenuItemFont::setFontSize(30);

	auto menuItemFirst = MenuItemFont::create("НОВА ГРА", CC_CALLBACK_1(MainMenu::GoToGameScene, this));

	auto menuItemSecond = MenuItemFont::create("Інфо", CC_CALLBACK_1(MainMenu::GoToInfoScene, this));

	auto menuCenter = Menu::create(menuItemFirst, menuItemSecond, NULL);
	menuCenter->setPosition(Vec2(size.width / 2.0, size.height / 2.3 -70));

	menuCenter->setColor(Color3B::WHITE);
	menuCenter->alignItemsVerticallyWithPadding(40.0f);

	this->addChild(menuCenter, 2);*/

//	ship = Sprite::create("ship.png");
//	ship->setScale(0.5);
//	ship->setPosition(Vec2(size.width / 2, 50));
//	this->addChild(ship);

	
	__String *fileName = __String::create("file2.plist");
	__String *filepath = __String::createWithFormat("%s%s", FileUtils::getInstance()->getWritablePath().c_str(), fileName->getCString());

	__Dictionary *dictionary1 = __Dictionary::createWithContentsOfFile(filepath->getCString());

	int s= dictionary1->valueForKey("Score")->intValue();
	auto digital = Label::createWithTTF(std::to_string(s), "fonts/Marker Felt.ttf", 50);
	digital->setPosition(Vec2(size.width / 2, size.height / 3.5 - 140));
	digital->setColor(Color3B(225, 170, 60));
	this->addChild(digital);

	auto gh = Sprite::create("gamehub.png");
    gh->setPosition(Vec2(50, 44));
    gh->setScale(0.25f);
    this->addChild(gh);


    return true;
}

/*void MainMenu::GoToGameScene(Ref *pSender)
{
	auto scene = GameScene::createScene();
	Director::getInstance()->replaceScene(scene);
}*/
void MainMenu::GoToChangeLevel(Ref *pSender)
{
	auto scene = ChangeLevel::createScene();
	Director::getInstance()->replaceScene(scene);
}
void MainMenu::GoToInfoScene(Ref *pSender)
{
	auto scene = InfoScene::createScene();
	Director::getInstance()->replaceScene(scene);
}


